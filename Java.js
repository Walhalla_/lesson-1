var sentence = 'cRiMinAl jUsTIce iS sMth whICh IS oFtEn dIScuSSed iN toDays wORlD';

var captializeString = (str) => str[0].toUpperCase() +
str.slice(1).toLowerCase();

var capitalizeWords = (str) => str.split(' ').map
(captializeString).join(' ');

console.log(capitalizeWords(sentence));